echo off
for /f %%i in ('npm root -g') do set VAR=%%i/data-model-editor-back
echo on
node %VAR%/dist/data-model-start-env.js %VAR%/jpa.restfull.jar %*

