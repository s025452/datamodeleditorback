** OBJECTIF

créer un server de  mock trés trés simple.
objectif reproduire un acte de gestion en mockant tous les appels.
évolution  permettre d'avoir  pour une entrée  VERBE URL QUESTION plusieur flux réponse qui seront retourné dans l'ordre d'apparition dans le fichier.
Pour permettre de rejouer un acte de gestion sans parallélisme pour simuler des mise à jours.


** INSTALL
npm install dadou-mock -g --save

** FORMAT FICHIER
VERBE
URL 
FLUX QUESTION ( si xml   xml{ "flux": < contenu du flux>} )
FLUX REPONSE ( si xml   xml{ "flux": < contenu du flux>} )
STATUS ( 200 , 201 etc ...)

** FONCTIONNEMENT

si il existe Un flux reponse qui correspond à l'url au flux QUESTION (si il non vide ) et au verbe on retour ne le flux reponse.


** Lancement
dadou-mock nomFichierContenantLesMock port


