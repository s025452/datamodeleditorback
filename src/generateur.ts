export interface Champ {
    nom: string,
    type: string
}

export interface Type {
    nom: string
    superType: string
    champs: Champ[]

}
export interface ApiSignature {
    typeQuestion: string,
    estUnique: boolean;
    typeReponse: string;
}

export interface Client {
    signatures: ApiSignature[]
    types: Type[]
}

function creerSourceType(type: string): string {
    if (type.startsWith("*")) {
        return `${creerSourceType(type.substr(1))}[]`
    }
    return type;
}
function creerSourceClassTs(type: Type, apiSignature: ApiSignature) {
    var result: string = `export class ${type.nom} `;
    if (type.superType) {
        result = `${result} extends ${type.superType}  { \n `
    } else {
        result = `${result} { \n`
    }
    if (type.champs) {
        type.champs.forEach((champ: Champ) => {
            result = `${result}\t${champ.nom}:${creerSourceType(champ.type)} \n`
        })
    }
    if (apiSignature) {
        if (apiSignature.typeReponse) {

            if (!apiSignature.estUnique) {

                result = `${result} convertirReponse( toObject:(json:any)=>any ,  reponse: any  ) : ${apiSignature.typeReponse}[]{\n`
                result = `${result}     return toObject(reponse);`

                result = `${result}}\n`
            } else {

                result = `${result} convertirReponse( toObject:(json:any)=>any ,  reponse: any  ) : ${apiSignature.typeReponse}{\n`
                result = `${result}     return toObject(reponse);`

                result = `${result}}\n`
            }
        }

    }
    result = `${result}}\n`
    return result;
}
const typeSchema = " { [nom: string]: { constructor: any, superType?: string, champs?: { [nom: string]: string } } } ";
function creerSourceSchema(types: Type[]): string {
    var resultat: string = `\nexport var schema : ${typeSchema} = \n{\n `
    var firstType = true;
    types.forEach((type) => {
        if (!firstType) {
            resultat += ',\n'
        }
        firstType = false
        resultat = `${resultat}\n\t${type.nom}:{\n\t\tconstructor:${type.nom}`
        if (type.superType) {
            resultat = `${resultat},\n\t\tsuperType:${type.superType}`
        }
        if (type.champs) {
            resultat = `${resultat},\n\t\tchamps:{`
            var first: boolean = true
            type.champs.forEach((champ: Champ) => {
                if (!first) {
                    resultat = `${resultat},`
                }
                resultat = `${resultat}\n\t\t\t${champ.nom}:"${champ.type}"`

                first = false;
            })
            resultat = `${resultat}}`
        }

        resultat = ` ${resultat}}`
    });
    resultat = ` ${resultat} } \n `
    return resultat
}
export function creerSourcePourClient(client: Client) {
    var result = ''
    client.types.forEach((type) => {
        result += creerSourceClassTs(type, client.signatures.find((apiSignature: ApiSignature) => {
            return apiSignature.typeQuestion === type.nom
        }));
    })
    result += creerSourceSchema(client.types)
    return result;
}
