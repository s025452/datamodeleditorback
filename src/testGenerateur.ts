export namespace Model {
    export class Bidule {
        nom: string
        titi: Titi
        constructor() {
            this.nom = "hello"
            //  this.titi = new Titi()
        }
    }
    var cls: any = Bidule
    var obj: any = new cls();
    console.log(obj.nom)
    export class Titi {

    }
    var titi = new Titi()
    var map: { [s: string]: Bidule } = {
        tit: new Bidule()
    }
    interface Drivers {
        jsonToObject:(json:any , type:string )=> any ,
        objectToJson:(json:any , type:string )=> any ,
        httpClient:(body: any , funRep:(reponse:any)=>void)=>void
    }
    export var schema: { [nom: string]: { constructor: any, superType?: string, champs?: { [nom: string]: string } } } = {
        Bidule: {
            constructor: Bidule,
            champs: {
                nom: 'string'
            }
        }
    }
    map.chose = new Bidule();
    console.log(titi.constructor === Titi)
    console.log(map.tit);
}
var bidule = new Model.Bidule()
console.log(Model.schema["Bidule"].champs.nom)
console.log(Model.schema["Bidule"].constructor)