import child_process from "child_process"
import * as axios from 'axios'
import fs from 'fs'

import {appeler} from './appelAxios';
import { creerRepertoire, lireJSON } from "./utils";
import * as generateur from './generateur'
import { creerOuModifierProjet } from "./generateur-fichier-projet";
console.log("demarage")

var modelFile = "C:\\Users\\david\\Documents\\jpa_rest_full\\src\\test\\resources\\test.model.json"

interface Appel {
    url:string ,
    body: any
}
var config = lireJSON("config.json",{})
console.log( `config = ${config.jar}`)



//creerRepertoire(config.jpaClassPath,model.url)
const jarPath = "C:\\Users\\david\\Documents\\jpa_rest_full\\target\\jpa.restfull-0.0.1-SNAPSHOT.jar"
const port = 8083
const headers:any = {}
var p= child_process.execFile(config.jvm, ["-jar", jarPath,modelFile,`${port}`,"test"
]);
headers["AdminKey"] = "test"
//var p=child_process.execFile("C:\\Program Files\\Java\\jdk-13\\bin\\java",["-jar", "D:\\git\\jpa_rest_full\\target\\jpa.restfull-0.0.1-SNAPSHOT.jar"]);
p.stderr.on( 'data' , ( data:any)=> {
    var s:string = data.toString();
   console.log(`${s}`); 
})
p.stdout.on('data', (data: any) => {
    var s:string = data.toString();
   console.log(`${s}`);
    
    if (s.startsWith("<Demarer>")) {
        var AdminKeyValue = null;
        console.log("Debut");

        appeler( [
            { url:`http://localhost:${port}/echo` , body:{}},
          
          /*  { url:'http://localhost:8080/test/persist' , body:{
                "type":"Personne",
                "valeur": {
                    "nom":"Toto",
                    "prenom":"Titi",
                    "salaire":45000
                }
                }},
            { url:'http://localhost:8080/test/api' , body:{
                type:"recupererPersonnes",
                valeur:{
                    valeur:500
                }
            }},*/
            { url:`http://localhost:${port}/client` , body:{} , action: (rep:any)=> {
            console.log("client")
            console.log(rep)    
            const client: generateur.Client = rep[0].valeur;
           
            creerOuModifierProjet('client',generateur.creerSourcePourClient(client),'c:/tmp/test-generation')
            fs.writeFileSync('c:/tmp/client.json',JSON.stringify(rep));
            p.kill();}}
        ],0 )
  
      }
  });
