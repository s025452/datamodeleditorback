import * as fs from 'fs';
import { ecrireJSON, lireJSON } from './utils';
function controler(chemin: string, type: string, estFichier: boolean) {

    if (!chemin) {
        throw `${type} non renseigné`
    }

    if (fs.existsSync(chemin)) {
        let s = fs.statSync(chemin);
        if (estFichier) {
            if (s.isFile()) {
                return;
            }
            throw `Fichier attendu pour ${type} `
        } else {
            if (s.isDirectory()) {
                return;
            }
            throw `Repertoire attendu pour ${type} `
        }
    }
    if (estFichier) {
        throw `Fichier inexistant pour la ${type}`
    } else {
        throw `Répertoire inexistant pour la ${type}`
    }


}


export function ecrireConfigJava(contenu: any) {
    controler(contenu.jvm, "Jvm", true);
    controler(contenu.jpaClassPath,"Jpa class path",false);
    const obj: any = lireJSON("config.json", {})
    obj.jvm = contenu.jvm
    obj.jpaClassPath = contenu.jpaClassPath
    ecrireJSON("config.json", obj)


}
export function ecrireConfigEnvironement(contenu: any) {
    const obj: any = lireJSON("config.json", {})
    obj.keyEnvironement = contenu.keyEnvironement
    obj.serveurEnvironement = contenu.serveurEnvironement
    ecrireJSON("config.json", obj)


}

export function ecrireConfigRepertoireTravail(contenu: any) {
    controler(contenu.dir, "repertoire travail", false);
    const obj: any = lireJSON("config.json", {})
    obj.dir = contenu.dir
    ecrireJSON("config.json", obj)

}