import * as path from "path";
import express, { Request, Response, response } from "express";
import * as bodyParser from "body-parser";
import fs from "fs";
import querystring from "querystring";
import * as connect from "connect";
function traiterQuestion() {
  return (question: Request, reponse: Response) => {

    //  reponse.setHeader("Content-Type", mock.contentTypeReponse);
    reponse.status(200).send({ valeur:"Echo"});
  }
}
// Creates and configures an ExpressJS web server.
export class AppJeux {
  // ref to Express instance
  public express: express.Application;
  //Run configuration methods on the Express instance.
  constructor(chemin: string) {
    this.express = express();


    this.middleware();
    if (chemin) {
      this.express.use('/', express.static(chemin));
    }
    this.express.route("/echo").get(traiterQuestion());

  }

  // Configure Express middleware.
  private middleware(): void {
    var router = express.Router();

    this.express.use(function (req: Request, res: Response, next) {
      // console.log("requete "+req.body.toString());
      //      console.log(req.headers);
      res.header("Access-Control-Allow-Origin", "*");
      //      res.header("Access-Control-Allow-Credentials", 'true');
      //      res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
      res.header(
        "Access-Control-Allow-Headers",
        "Origin,X-Requested-With,Content-Type,Accept,content-type,application/json,key,user"
      );
      next();
    });

    this.express.use(bodyParser.text());
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
  }
}
