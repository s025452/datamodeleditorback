import child_process from "child_process"
import * as axios from 'axios'
import fs from 'fs'

//var model: any = fs.readFileSync("D:\\git\\jpa_rest_full\\src\\test\\resources\\test.model.json")
interface Appel {
    url:string ,
    body: any,
    headers?:any,
    action?: (body:any)=>void
    fail?: (body:any) => boolean 
}

export function appeler( appels: Appel [] , idx: number ) {
    if (idx  === appels.length) {
        console.log("Fin ...")
        return
   
    }
    let config = {
        headers: {
        },
        timeout: 3000000
      }
    if (appels[idx].headers) {
        config.headers = appels[idx].headers
    }  
    const appel = appels[idx]
    axios.default.post(appel.url,appel.body,config).then( (rep) => {
        
        console.log(`url =${appel.url} `)
        if (appel.action) {
            appel.action(rep.data);
        } else {
        console.log(rep.data)
        }
        appeler(appels,idx+1);
    }).catch(error => {
        if (appels[idx].fail) {
            if (appels[idx].fail(error)) {
                appeler(appels,idx+1); 
            }
        } else {
        console.log(error);
        }
    });

}