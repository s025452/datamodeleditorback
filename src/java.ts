import express, { Request, Response, response } from "express";
import * as bodyParser from 'body-parser';
import * as fs from 'fs';
import * as os from 'os';
import * as axios from 'axios'
import { appeler } from './appelAxios';
import { Type, TypeService } from 'generator-666';
import child_process from "child_process"
import { lireJSON } from "./utils";
import { App } from "./server";
export class JvmRegistry {
  private map: Map<string, Jvm> = new Map();
  public portCourant: number = 8080
  public jpaJar: string
  constructor(portInit: number, jpaJar: string) {
    this.portCourant = portInit;
    this.jpaJar = jpaJar;
  }
  recupererJvmPour(nomModel: string) {
    let jvm: Jvm = this.map.get(nomModel)
 
    return jvm
  }
  lancerJvmPour(nomModel: string , callback: (port:number)=>void) {
    let jvm: Jvm = this.map.get(nomModel)
    var port: number
    if (jvm) {
      jvm.javaProcess.kill()
      port = jvm.port;
     
    
      
    } else {
      port = this.portCourant
      this.portCourant ++
    }
    jvm = new Jvm(this.jpaJar,nomModel,port,callback)
    this.map.set(nomModel, jvm)
    return jvm
  }

}

export class Jvm {
  public port: number = 8080
  public javaProcess: child_process.ChildProcess
  public adminKeyValue: string
  public jpaJar: string
  public modelFile: string
  constructor(jpaJar: string, modelFile:string, port:number , callback:( port:number)=>void) {
    this.jpaJar = jpaJar;
    this.modelFile = modelFile ;
    let config = lireJSON("config.json", {});
    this.adminKeyValue ="dev"
    let javaJvm = config.jvm
    this.port = port;
    this.javaProcess = child_process.execFile(javaJvm, ["-jar", this.jpaJar, modelFile ,`${port}`,"dev"]);
    this.javaProcess.stdout.on('data', (data: any) => {
      var s: string = data.toString();
      console.log(`${s}`);

      if (s.startsWith("<Demarer>")) {
        callback(port);
      }})

  }
  updateApi(install: any, res: Response) {
    appeler([

      {
        url: `http://localhost:${this.port}/api/update`, body: install, headers: { "AdminKey": this.adminKeyValue }, action: (rep: any) => {
          const typeService: TypeService = new TypeService();
          //const types: Type[] = rep[0].valeur.types;
          //typeService.init(types);
          // let src = typeService.creerSourceAvecReferences();
          //  fs.writeFileSync(`c:/tmp/${install.model.url}.client.json`, JSON.stringify(types))
          // fs.writeFileSync(`c:/tmp/${install.model.url}.client.ts`, src)
          res.status(200).send({ erreurs: rep[0].valeur.erreurs });
          console.log("Generation ok")
        }
      }
    ], 0)
  }
  installProxy(jvm: Jvm, model: any, express: express.Application) {

    express.route(`/data/${model.url}/client`).get((reqData: Request, resData: Response) => {
      axios.default.post(`http://localhost:${jvm.port}/client`).then((rep) => {
        resData.status(200).send(rep.data);
      })

    })
    express.route(`/data/${model.url}/persist/:nom`).post((reqData: Request, resData: Response) => {

      let config: any = {
        headers: {

        }
      }
      if (reqData.headers['adminkey']) {

        config.headers['adminkey'] = reqData.headers['adminkey']
      }
      axios.default.post(`http://localhost:${jvm.port}/persist`, {
        type: reqData.params['nom'],
        valeur: reqData.body
      }, config).then((rep) => {
        resData.status(200).send(rep.data);
      }).catch((reason: any) => {
        resData.status(500).send({
          url: `http://localhost:${jvm.port}/persist/${reqData.params['nom']}`,
          body: reqData.body,
          reason: reason.response.data
        });
      })

    })
    express.route(`/data/${model.url}/api/:nom`).post((reqData: Request, resData: Response) => {
      let config: any = {
        headers: {

        }
      }
      if (reqData.headers['adminkey']) {

        config.headers['adminkey'] = reqData.headers['adminkey']
      }
      axios.default.post(`http://localhost:${jvm.port}/api`, {
        type: reqData.params['nom'],
        valeur: reqData.body
      }, config).then((rep) => {
        resData.status(200).send(rep.data);
      }).catch((reason: any) => {
        resData.status(reason.response.status).send({
          url: `http://localhost:${jvm.port}/api/${reqData.params['nom']}`,
          body: reqData.body,
          reason: reason.response.data
        });
      })

    })
  }

}
