import * as os from 'os';
import * as fs from 'fs';

export function creerRepertoire(base: string , nom: string) {
  const repertoire: string = `${base}/${nom}`
  if (fs.existsSync(repertoire)) {
    let s = fs.statSync(repertoire);
    if (s.isFile()) {
      throw `${repertoire} est un fichier`
    }
    return;
  }
  fs.mkdirSync(repertoire);

}
export function lireJSON(nomFichier: string, valeurParDefaut: any) {
    let cheminCompletFichier = `${os.homedir()}/${nomFichier}`;
    let contenu = valeurParDefaut
    if (fs.existsSync(cheminCompletFichier)) {
      let s = fs.statSync(cheminCompletFichier);
      if (s.isFile()) {
        contenu = JSON.parse(fs.readFileSync(cheminCompletFichier).toString());
      }
    }
    return contenu
  }
export function ecrireJSON( nomFichier: string , contenu: any ): any {
    let cheminCompletFichier = `${os.homedir()}/${nomFichier}`;
    fs.writeFileSync(cheminCompletFichier,JSON.stringify(contenu))
}  