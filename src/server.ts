import express, { Request, Response, response } from "express";
import * as generateur from './generateur'
import * as bodyParser from 'body-parser';
import * as fs from 'fs';
import * as axios from 'axios'
import { Jvm , JvmRegistry} from './java'
import { creerRepertoire, ecrireJSON, lireJSON } from "./utils";
import {ecrireConfigEnvironement, ecrireConfigJava, ecrireConfigRepertoireTravail } from "./config";
import { appeler } from "./appelAxios";
import { creerOuModifierProjet, genererProjet } from "./generateur-fichier-projet";
const fichierAssociation = 'association.json'
    
function normaliserChemin( chemin:string ) {
  return chemin.split("\\").join('/')
}
export class App {

  // ref to Express instance
  public express: express.Application;

  public adminKeyValue: string
  public jvmRegistry: JvmRegistry ; 
  public modelCourant: string ;  

  
  //Run configuration methods on the Express instance.
  constructor(port: number ,cheminFront: string , jpaJar: string) {
    this.express = express();
    
    this.jvmRegistry = new JvmRegistry(port+1,jpaJar);
    this.middleware();
    if (cheminFront) {
      this.express.use('/', express.static(cheminFront));
    }

    this.express.route("/config").get((req: Request, res: Response) => {
      var config : any = lireJSON('config.json', {})
 
      res.status(200).send(config);
    });

    this.express.route("/config/java").post((req:Request,res:Response) => {
      try {
      ecrireConfigJava( req.body );
      res.status(200).send({ message:"Mise à jour ok"});
      } catch( erreur ) {
        res.status(500).send({ message:erreur});

      }
    })
    this.express.route("/config/environement").post((req:Request,res:Response) => {
      try {
      ecrireConfigEnvironement(  req.body);
      res.status(200).send({ message:"Mise à jour ok"});
      } catch( erreur ) {
        res.status(500).send({ message:erreur});

      }
    })
    this.express.route("/config/dir").post((req:Request,res:Response) => {
    
      try {
      ecrireConfigRepertoireTravail( req.body);
      res.status(200).send({ message:"Mise à jour ok"});
      } catch( erreur ) {
        res.status(500).send({ message:erreur});

      } 
    })
    this.express.route("/supprimer").post((req: Request, res: Response) => {
      const ressource = `${req.body}`
      const s = fs.statSync(ressource);
      if (s.isFile()) {
        console.log(`supprimer file ${ressource} `);
        fs.unlinkSync(ressource);
      }
      if (s.isDirectory) {
        console.log(`supprimer directory ${ressource} `);
        fs.rmdirSync(ressource);
      }
    });
    this.express.route("/type").post((req: Request, res: Response) => {

      const ressource = `${req.body}`

      if (fs.existsSync(ressource)) {
        const s = fs.statSync(ressource);
        if (s.isFile()) {
          console.log(`type ${ressource} file`);
          res.status(200).send({ type: 'file' });
          return;
        }
        if (s.isDirectory()) {
          console.log(`type ${ressource} directory`);
          res.status(200).send({ type: 'directory' });
          return;
        }
      }
      console.log(`type ${ressource} non existant`);
      res.status(200).send({});
    });
    this.express.route("/lire").post((req: Request, res: Response) => {

      const ressource = `${req.body}`
      console.log(`lire ${ressource}`);
      if (fs.existsSync(ressource)) {
        const s = fs.statSync(ressource);
        if (s.isFile()) {
          res.status(200).send({ type: 'file', data: fs.readFileSync(ressource).toString() });
          return;
        }
        if (s.isDirectory()) {
          const ls = fs.readdirSync(ressource);
          const lsEtType = ls.map((val) => {
            const enfant = ressource + '/' + val;
            const se = fs.statSync(enfant);
            let type = '?';
            if (se.isDirectory()) {
              type = 'directory';
            }
            if (se.isFile()) {
              type = 'file';
            }
            return { type: type, name: val }
          });
          res.status(200).send({ type: 'directory', data: lsEtType });
          return;
        }
      }
      res.status(200).send({});

    });

    this.express.route("/ecrire").post((req: Request, res: Response) => {
      let body: any = req.body;
      const jsonObject = body;
      const ressource = `${jsonObject.ressource}`;
      if (jsonObject.type === 'file') {
        //        console.log(`ecrire file ${ressource}\n${jsonObject.contenu}`);

        console.log(`ecrire file ${ressource}`);
        fs.writeFileSync(ressource, jsonObject.contenu);
      }
      if (jsonObject.type === 'directory') {
        console.log(`ecrire directory ${ressource}\n`);
        fs.mkdirSync(ressource);
      }
      res.status(200).send({});
    });
    this.express.route("/update_api").post((req: Request, res: Response) => {
      let body: any = req.body;
      const jsonObject = body;
      const ressource: string = jsonObject.ressource;
      var modelSource: string = fs.readFileSync(ressource).toString()
      var model: any = JSON.parse(modelSource.toString()) 
      var config: any = lireJSON("config.json",{})
      var install = {
        model: model,
        cheminGeneration: `${config.jpaClassPath}/${model.url}`.split("\\").join( "/") 
      }

      let jvm: Jvm = this.jvmRegistry.recupererJvmPour(ressource);
  
      jvm.updateApi(install, res);




    });
    this.express.route("/model-courant").get((req: Request , res: Response) => {
      if (this.modelCourant) {
        res.status(200).send({ ressource:this.modelCourant })
        return;
      }
      res.status(200).send({ ressource:'' })
    })
    this.express.route("/model-courant").post((req: Request , res: Response) => {
      const ressource:string = req.body.ressource
      const fichierAssociation = 'association.json'
      var obj =lireJSON(fichierAssociation, {})
      obj[this.modelCourant] = normaliserChemin(ressource);
      ecrireJSON(fichierAssociation,obj)
      res.status(200).send({  })
    })
    this.express.route("/model-association").get((req: Request , res: Response) => {
      var obj =lireJSON(fichierAssociation, {})
      res.status(200).send(obj)
    })
    this.express.route("/generer_client").post((req: Request, res: Response) => {
      let body: any = req.body;
      const jsonObject = body;
      const ressource = jsonObject.ressource;
      const cible: any = null
      this.modelCourant = ressource;
      this.modelCourant =  normaliserChemin(this.modelCourant)
      var obj =lireJSON(fichierAssociation, {})
      var generationRep: string = obj[this.modelCourant];
     this.jvmRegistry.lancerJvmPour(ressource,(port)=> {
        appeler([

          {
            url: `http://localhost:${port}/client`, body: {}, headers: { "AdminKey": "dev" }, action: (rep: any) => {
              const client: generateur.Client = rep[0].valeur;
              if (generationRep) {
                const nomProjet: string = this.modelCourant.substr(this.modelCourant.lastIndexOf('/')+1);
                creerOuModifierProjet(nomProjet,generateur.creerSourcePourClient(client),generationRep)
              }
              res.status(200).send({ erreurs: rep[0].valeur.erreurs });
              console.log("Generation ok")
            }
          }
        ], 0)
      })
     
      

    });



    //this.routes();
  }

  // Configure Express middleware.
  private middleware(): void {
    var router = express.Router();

    this.express.use(function (req, res, next) {
      res.header("Access-Control-Allow-Origin", '*');

      res.header("Access-Control-Allow-Headers", 'Origin,X-Requested-With,Content-Type,Accept');
      next();
    });


    this.express.use(bodyParser.json());
    this.express.use(bodyParser.text());
    this.express.use(bodyParser.urlencoded({ extended: false }));
  }



}

export function start(port: number, cheminFront: string , jpaJar: string) {
  new App(port,cheminFront,jpaJar).express.listen(port, () => {
    console.log('listening on port ' + port);
  });
}
