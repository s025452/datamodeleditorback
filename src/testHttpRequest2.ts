import child_process from "child_process"
import * as axios from 'axios'
import fs from 'fs'

import { appeler } from './appelAxios';
import { creerRepertoire, lireJSON } from "./utils";
var model: any = fs.readFileSync("C:\\Users\\david\\Documents\\alimentation-sport-sante-data-model\\data.v2.model.json")
var config = lireJSON("config.json", {})
var model = JSON.parse(model.toString())

var install = {
    model: model,
    cheminGeneration: `${config.jpaClassPath}`.replace("\\", "/")
}
creerRepertoire(config.jpaClassPath, model.url)

const headers: any = {}
appeler([
    { url: 'http://localhost:8080/echo', body: {} },
    {
        url: 'http://localhost:8080/install', body: install, action: (rep: any) => {
            headers["AdminKey"] = rep

        }
    },
    {
        url: 'http://localhost:8080/application/persist', body: {
            type: 'IngredientOpenFoodFact',
            valeur: {
                code: '999',
                donnees: '{}'
            }
        } ,
        fail: (error)=> {
            console.log(error.response.data)
            return false
        }
    },
    {
        url: 'http://localhost:8080/application/api', body: {
            type: 'ingredientPourCodeBar',
            valeur: {
                code: '999'
            }
        } ,
        fail: (error)=> {
            console.log(" erreur" + error.response.data)
            return false
        }
    }/*,
    { url:'http://localhost:8080/application/api/update' , body:install , headers:headers , action: (rep:any)=> { 
        console.log(rep)     
    fs.writeFileSync('c:/tmp/client.install.api.json',JSON.stringify(rep))}}*/
], 0)

